﻿$(function () {

    $('#chatBody').hide();
    $('#loginBlock').show();

    // Ссылка на автоматически-сгенерированный прокси хаба
    var chat = $.connection.chatHub;

    // Объявление функции, которая хаб вызывает при получении сообщений
    chat.client.addMessage = function (name, message) {
        // Добавление сообщений на веб-страницу 
        $('#chatroom').prepend('<tr><td><b class="label label-info">' + htmlEncode(name) + '</b>: ' + htmlEncode(message) + '</td></tr>');
    };

    // Функция, вызываемая при подключении нового пользователя
    chat.client.onConnected = function (id, userName, allUsers) {

        $('#loginBlock').hide();
        $('#chatBody').show();

        // установка в скрытых полях имени и id текущего пользователя
        $('#hdId').val(id);
        $('#username').val(userName);
        $('#header').html('<div class="alert alert-success row">Добро пожаловать, ' + userName + '</div>');

        // Добавление всех пользователей
        for (i = 0; i < allUsers.length; i++) {
            AddUser(allUsers[i].ConnectionId, allUsers[i].Name);
        }

        $('#message').focus();
    }

    // Добавляем нового пользователя
    chat.client.onNewUserConnected = function (id, name) {

        AddUser(id, name);
    }

    // Удаляем пользователя
    chat.client.onUserDisconnected = function (id, userName) {

        $('#' + id).remove();
    }

    // Открываем соединение
    $.connection.hub.start().done(function () {

        $('#sendmessage').click(function () {
            if ($.trim($('#message').val()) != '') {
                // Вызываем у хаба метод Send
                chat.server.send($('#username').val(), $('#message').val());
                $('#message').val('');
            }

            $('#message').focus();
        });

        // обработка логина
        $("#btnLogin").click(function () {

            var name = $("#txtUserName").val();
            if (name.length > 0) {
                chat.server.connect(name);
            }
            else {
                alert("Введите имя");
            }
        });

    });

    $("#txtUserName").keypress(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $("#btnLogin").click();
        }
    });

    $('#message').keypress(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $('#sendmessage').click();
        }
    });

});

// Кодирование тегов
function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}

//Добавление нового пользователя
function AddUser(id, name) {

    var userId = $('#hdId').val();

    if (userId != id) {

        $("#chatuserslist").append('<li id="' + id + '"><p class="label label-info" ><b>' + name + '</b></p></li>');
    }
}